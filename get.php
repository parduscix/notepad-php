<?php 
include("./ctrl.php");
$files = array_diff(scandir("msgs"), array('.', '..'));
echo '<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
<channel>
<title>NotePad</title>
<description>Notepad RSS</description>
<lastBuildDate>'.date('D, d M Y h:i:s T').'</lastBuildDate>
<pubDate>'.date('D, d M Y h:i:s T').'</pubDate>
';
foreach($files as $file){
	echo "\t<item>\n\t\t<title>";
	include("tls/".$file);
	echo "</title>\n\t\t<description>";
	include("msgs/".$file);
	echo "</description>\n\t\t<lastBuildDate>";
	include("dts/".$file);
	echo "</lastBuildDate>\n\t\t<pubDate>";
	include("dts/".$file);
	echo "</pubDate>\n\t\t<guid>".str_replace(".php","",$file)."</guid>\n\t</item>";
	

}
echo '
</channel>
</rss>';
?>
